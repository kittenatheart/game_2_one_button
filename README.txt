Goal, intent, intended tone, or metaphor/why?:
This game is meant to be one you can play to pass the time.
It is meant to feel a bit like an escape game but with very low stakes.


Theme inspiration:
Because we could only use one button as an input, I wanted to make every piece of the button-holding process matter.
Because of the simple controls, I wanted a simple feeling and looking game.


Controls:
Use spacebar or left mouse click (whichever is more comfortable)
- click to stop rotation in the direction your green aiming piece is pointing
- hold as long as you like: the longer you hold, the farther you go
- release to move


Things you need to know:
Don't direct yourself off the screen. There is an issue with the walls, and they don't stop you right now.



G390 Game 2
Spring 2021
Shannon Master