﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private bool stage1 = true;
    private bool stage2;
    private float moveCount;
    private int curCount = 1;
    private Vector3 target;

    //special bools for position facing
    private bool right;
    private bool up;
    private bool left;
    private bool down;

    public SpriteRenderer lineSprite;
    public SpriteRenderer lineSprite2;
    public Text moveText;

    void Start()
    {

    }

    void Update()
    {
        //hit key, stop the spinning
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            stage1 = false;
            lineSprite.enabled = false;
            stage2 = true;
            moveText.text = "Moving...";
        }
        //Hold key, increase moveCount
        if (Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0))
        {
            moveCount += .01f;
            if(moveCount >= curCount)
            {
                moveText.text = "Moving " + curCount;
                curCount++;
            }
        }
        //let go, move moveCount number of spaces
        if (Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonUp(0))
        {
            moveCount = curCount - 1;
            curCount = 1;
            lineSprite2.enabled = false;
            moveText.text = null;

            //move to new position
            StartCoroutine("Mover");


            //reset for next stage 1
            lineSprite.enabled = true;
            lineSprite2.enabled = true;
            stage1 = true;
            stage2 = false;
            moveCount = 0;
        }

        //spin
        if (stage1)
        {
            transform.Rotate(new Vector3(0, 0, -90) * Time.deltaTime);
        }
    }
    
    //moves player in the direction they are facing
    private IEnumerator Mover()
    {
        Debug.Log("moving now");
        //find what quadrant we are facing, where to go
        if (right)
        {
            Debug.Log("going right");
            target = new Vector3(transform.position.x + moveCount, transform.position.y, 0);
        }
        else if(up)
        {
            Debug.Log("going up");
            target = new Vector3(transform.position.x, transform.position.y + moveCount, 0);
        }
        else if(left)
        {
            Debug.Log("going left");
            target = new Vector3(transform.position.x - moveCount, transform.position.y, 0);
        }
        else
        {
            Debug.Log("going down");
            target = new Vector3(transform.position.x, transform.position.y - moveCount, 0);
        }

        transform.position = target;

        yield return new WaitForSeconds(3f);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Right"))
        {
            right = true;
        }
        else if (other.CompareTag("Up"))
        {
            up = true;
        }
        else if (other.CompareTag("Left"))
        {
            left = true;
        }
        else if (other.CompareTag("Down"))
        {
            down = true;
        }
        else if (other.CompareTag("Finish"))
        {
            moveText.text = "Great Job!";
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("Right"))
        {
            right = false;
        }
        else if (other.CompareTag("Up"))
        {
            up = false;
        }
        else if (other.CompareTag("Left"))
        {
            left = false;
        }
        else if (other.CompareTag("Down"))
        {
            down = false;
        }
    }
}
